import binascii
import os

'''
    Universidad de Costa Rica
    IE-0527: Inegnieria de Comunicaciones
    Simulation Project: Fuente 

    Project group: Pypass

        Abstract: Simulation project that converts an input file from it's original format, to a binary codificated format [file2bin function], it passes the codificated message to a function that transforms from binary to the same format as the input [bin2file function] in order to check it's similarities.

        Requierements:
            1) Python3 for running
            2) Change the hardcoded path in the main function to your desired test files.
            3) Use either Windows format or Linux format to specify the path. Comment the other one.
            
        Notes: 
            -This program needs a hardcoded input from the user to the input file (to transform) 
            -This program creates 2 different outputs: 
                -output_bin.txt: file with the binary codification of the input file
                -output_file.[extension]: file re-transformed to the same extension as the input

        Block diagram of the flow:
     ----------                               --------------                    -----------
    |          |     -------------------     |              |    ---------     |           |
    |input_file|---->Binary codification---->|Binary message|---->Decoding---->|output_file|
    |          |     -------------------     |              |    ---------     |           |
     ----------                               --------------                    -----------
        |                                          |                                 |
        |                                          |                                 |
        ---->from user (in main function)          ---->Saved to output_bin.txt      ---->Saved to output_file
'''



def file2bin(input_file, output_bin):
    '''
        Creates a binary file as output from a given input file with no particular format.
            Inputs:
                -input_file: [str] source file to transfer (any extension will work [eg: .jpg, .png, .mp3, .pptx, ...])
                -output_bin: [str] binary file created from the input file codification.

            Outputs:
                -bin_string: [str] binary string with the information of the input file.
    '''

    # Definition of files handler.
    writer = open(output_bin, 'w')

    # reading the contents of the input file 
    with open(input_file, 'rb') as f:
        content = f.read()

    # Changing the contents from the input file format to hex codification.
    bin_string = binascii.hexlify(content)
    writer.write(f'{bin_string}')
    print(f'[INFO] Hex file successfully created as: {output_bin}')

    # Closing files.
    f.close()
    writer.close()
    
    return bin_string

def bin2file(bin_string, output_file):

    '''
    Converts from an input hex file to a selected format output file. 
        Inputs:
            -bin_string: [str] binary string received from an external source.
            -output_file: [str] result of the re transformation of binary data to a particular file.
    '''

    with open(output_file, 'wb') as fout:
        # Re-transforming the binary string to data  
        fout.write(binascii.unhexlify(bin_string))
        fout.close()

    print('[INFO] New file created succesfully')

def extension_handler(input_file):
    '''
        This function gets the extension from the input file and creates the output_file with the same extension
            Inputs:
                -input_file: [str] hardcoded input file from the main function (including its extension)
            Outputs: 
                -output_bin: [str] path and name (hardcoded) to the output binary string to transmit
                -output_file [str] path and name (with the same extension as the input) to the received file.
    '''
    # Splitting the path given as input
    path = os.path.split(input_file)
    output_path = path[0]
    extension = path[1]
    # Getting the extension of the file given as input
    extension = extension.split('.')
    extension = extension[1]

    # Output paths definition
    output_bin = os.path.join(output_path, 'output_bin.txt')
    output_file = os.path.join(output_path, f'output_file.{extension}')

    return output_bin, output_file

def main():
    # Hardcoded path to the input file (WINDOWS FORMAT)
    input_file = r'C:\Users\Desktop\test.mp3'

    # Hardcoded path to the input file (LINUX FORMAT)
    input_file = './test.mp3'

    # setting the output files to the same location and extension as the input file
    output_bin, output_file = extension_handler(input_file)

    # Transforming the input file to binary
    bin_string = file2bin(input_file, output_bin)

    # Transforming the binary string to an output file
    bin2file(bin_string,output_file)


if __name__ == '__main__':
    main()


